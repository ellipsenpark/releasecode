# parkellipsen - cafetera (2025)

from Optimot:

```
cafetera 
1 1 f. [ED] [LC] Recipient per a servir el cafè. 
1 2 f. [ED] Màquina per a fer la infusió de cafè. Cafetera elèctrica. 
1 3 [ED] [HO] [LC] cafetera exprés Cafetera que permet fer cafè a l'instant dipositant gra de cafè molt, 
    ben fi, en un filtre i sotmetent-lo al pas d'aigua a alta pressió i temperatura. 
2 1 f. [LC] Objecte vell, màquina que funciona malament. 
2 2 f. [LC] Persona revellida, inhàbil, maldestra. 
```

A good day (for me) starts with a cup of coffee and some live coding. These seven tracks are a one-week-long
snapshot of that practice, live-coding a from-scratch session with a cup of coffee, soon after waking up.

Sometimes I'm not quite awake yet, sometimes ideas can drift apart, sometimes grow together again,
sometimes not. The style is eclectic.

The coding is more relaxed than in front of an audience, given the situation and time of day. The tracks
typically end up around 12 minutes. No multitrack recording has been used, the tracks have been created as one
whole piece. Some light mastering has been applied

Created with Mégra.rs (https://github.com/the-drunk-coder/megra.rs)

You can find the code included in this release.

Produced with free and open-source software (Ardour, Calf Compressor & Eq, Luftikus Eq).

Field recordings from Barcelona and Maine.

(c) 2025 Niklas Reppel






