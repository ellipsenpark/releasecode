# ReleaseCode

This repository contains the code for my music releases and notable live performances, typically in their post-performance state.

Typical publishing places:

* https://see.ellipsenpark.de
* https://ellipsenpark.bandcamp.com
* https://www.youtube.com/channel/UCUTLFofI27Uch-Hj82pZ6zQ
* https://hear.ellipsenpark.de
* https://soundcloud.com/parkellipsen (fading out)

It contains the code but not the samples, as I regard those as somewhat personal (especially the ones that I recorded myself over the course of my lifetime). So you'll have to find replacements for those, which gives you the chance to create new, interesting textures!
